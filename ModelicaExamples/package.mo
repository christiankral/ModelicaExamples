within ;
package ModelicaExamples
extends Modelica.Icons.ExamplesPackage;

annotation (uses(Modelica(version="3.2.3"), Complex(version="3.2.3")));
end ModelicaExamples;
