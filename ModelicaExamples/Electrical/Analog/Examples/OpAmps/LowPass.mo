within ModelicaExamples.Electrical.Analog.Examples.OpAmps;
model LowPass "Low-pass filter"
  extends Modelica.Icons.Example;
  import Modelica.Constants.pi;
  parameter Modelica.SIunits.Voltage Vps=+15 "Positive supply";
  parameter Modelica.SIunits.Voltage Vns=-15 "Negative supply";
  parameter Modelica.SIunits.Voltage Vin=5 "Amplitude of input voltage";
  parameter Modelica.SIunits.Frequency f=10 "Frequency of input voltage";
  parameter Real k=1 "Desired amplification";
  parameter Modelica.SIunits.Resistance R1=1000 "Arbitrary resistance";
  parameter Modelica.SIunits.Resistance R2=k*R1 "Calculated resistance to reach k";
  parameter Modelica.SIunits.Frequency fG=f/10 "Limiting frequency, as an example coupled to f";
  parameter Modelica.SIunits.Capacitance C=1/(2*pi*fG*R2) "Calculated capacitance to reach fG";
  Modelica.Electrical.Analog.Ideal.IdealizedOpAmpLimted opAmp(
    Vps=Vps,
    Vns=Vns,
    v_in(start=0))
    annotation (Placement(transformation(extent={{0,-10},{20,10}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-20,-100},{0,-80}})));
  Modelica.Electrical.Analog.Sources.TrapezoidVoltage vIn(
    rising=0.2/f,
    width=0.3/f,
    falling=0.2/f,
    period=1/f,
    nperiod=-1,
    startTime=-(vIn.rising + vIn.width/2),
    V=Vin,
    offset=0) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-80,0})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor vOut annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={50,-20})));
  Modelica.Electrical.Analog.Basic.Resistor r1(R=R1)
    annotation (Placement(transformation(extent={{-40,20},{-20,40}})));
  Modelica.Electrical.Analog.Basic.Resistor r2(R=R2)
    annotation (Placement(transformation(extent={{20,20},{0,40}})));
  Modelica.Electrical.Analog.Basic.Capacitor c(C=C, v(fixed=true, start=0))
    annotation (Placement(transformation(extent={{20,40},{0,60}})));
equation
  connect(r1.n, r2.n) annotation (Line(
      points={{-20,30},{0,30}}, color={0,0,255}));
  connect(r2.n, opAmp.in_n) annotation (Line(
      points={{0,30},{-10,30},{-10,6},{0,6}}, color={0,0,255}));
  connect(r2.p, opAmp.out) annotation (Line(
      points={{20,30},{30,30},{30,0},{20,0}}, color={0,0,255}));
  connect(r2.p, c.p) annotation (Line(
      points={{20,30},{30,30},{30,50},{20,50}}, color={0,0,255}));
  connect(c.n, r2.n) annotation (Line(
      points={{0,50},{-10,50},{-10,30},{0,30}}, color={0,0,255}));
  connect(ground.p, opAmp.in_p) annotation (Line(
      points={{-10,-80},{-10,-6},{0,-6}}, color={0,0,255}));
  connect(vIn.p, r1.p) annotation (Line(
      points={{-80,10},{-80,30},{-40,30}}, color={0,0,255}));
  connect(ground.p, vIn.n) annotation (Line(
      points={{-10,-80},{-80,-80},{-80,-10}}, color={0,0,255}));
  connect(opAmp.out, vOut.p) annotation (Line(
      points={{20,0},{50,0},{50,-10}}, color={0,0,255}));
  connect(ground.p, vOut.n) annotation (Line(
      points={{-10,-80},{50,-80},{50,-30}}, color={0,0,255}));
  annotation (Documentation(info=
                 "<html>
                         <p>This is a (inverting) low pass filter. Resistance R1 can be chosen, resistance R2 is defined by the desired amplification k, capacitance C is defined by the desired cut-off frequency.</p>
                         <p>The example is taken from: U. Tietze and C. Schenk, Halbleiter-Schaltungstechnik (German), 11th edition, Springer 1999, Chapter 13.3</p>
                         </html>"),
    experiment(
      StartTime=0,
      StopTime=1,
      Tolerance=1e-006,
      Interval=0.001));
end LowPass;
