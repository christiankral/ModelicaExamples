within ModelicaExamples.Electrical.Analog.Examples.OpAmps;
model Differentiator "Differentiating amplifier"
  extends Modelica.Icons.Example;
  import Modelica.Constants.pi;
  parameter Modelica.SIunits.Voltage Vps=+15 "Positive supply";
  parameter Modelica.SIunits.Voltage Vns=-15 "Negative supply";
  parameter Modelica.SIunits.Voltage Vin=5 "Amplitude of input voltage";
  parameter Modelica.SIunits.Frequency f=10 "Frequency of input voltage";
  parameter Real k=2 "Desired amplification";
  parameter Modelica.SIunits.Resistance R=1000 "Arbitrary resistance";
  parameter Modelica.SIunits.Capacitance C=k/(2*pi*f*R) "Calculated capacitance to reach desired amplification k";
  Modelica.Electrical.Analog.Ideal.IdealizedOpAmpLimted opAmp(Vps=Vps, Vns=
        Vns) annotation (Placement(transformation(extent={{0,-10},{20,10}})));
  Modelica.Electrical.Analog.Basic.Ground ground
    annotation (Placement(transformation(extent={{-20,-100},{0,-80}})));
  Modelica.Electrical.Analog.Sources.TrapezoidVoltage vIn(
    V=2*Vin,
    rising=0.2/f,
    width=0.3/f,
    falling=0.2/f,
    period=1/f,
    nperiod=-1,
    offset=-Vin,
    startTime=-(vIn.rising + vIn.width/2)) annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-80,0})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor vOut annotation (Placement(
        transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={50,-20})));
  Modelica.Electrical.Analog.Basic.Resistor r(R=R)
    annotation (Placement(transformation(extent={{20,20},{0,40}})));
  Modelica.Electrical.Analog.Basic.Capacitor c(C=C, v(fixed=true, start=0))
    annotation (Placement(transformation(extent={{-40,20},{-20,40}})));
equation
  connect(r.n, c.n) annotation (Line(
      points={{0,30},{-20,30}}, color={0,0,255}));
  connect(vIn.p, c.p) annotation (Line(
      points={{-80,10},{-80,30},{-40,30}}, color={0,0,255}));
  connect(vIn.n, ground.p) annotation (Line(
      points={{-80,-10},{-80,-80},{-10,-80}}, color={0,0,255}));
  connect(ground.p, opAmp.in_p) annotation (Line(
      points={{-10,-80},{-10,-6},{0,-6}}, color={0,0,255}));
  connect(opAmp.in_n, r.n) annotation (Line(
      points={{0,6},{-10,6},{-10,30},{0,30}}, color={0,0,255}));
  connect(r.p, opAmp.out) annotation (Line(
      points={{20,30},{30,30},{30,0},{20,0}}, color={0,0,255}));
  connect(opAmp.out, vOut.p) annotation (Line(
      points={{20,0},{50,0},{50,-10}}, color={0,0,255}));
  connect(vOut.n, ground.p) annotation (Line(
      points={{50,-30},{50,-80},{-10,-80}}, color={0,0,255}));
  annotation (Documentation(info=
                 "<html>
                        <p>This is a (inverting) differentiating amplifier. Resistance R can be chosen, capacitance C is defined by the desired time constant resp. frequency.</p>
                        </html>"),
    experiment(
      StartTime=0,
      StopTime=1,
      Tolerance=1e-006,
      Interval=0.001));
end Differentiator;
