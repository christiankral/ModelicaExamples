within ModelicaExamples.Electrical.PowerConverters.Examples.DCDC.ChopperStepUp;
model ChopperStepUp_R "Step up chopper with resistive load"
  extends ExampleTemplates.ChopperStepUp;
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Resistance R=10 "Resistance";
  Modelica.Electrical.Analog.Basic.Resistor resistor(R=R) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={40,50})));
equation
  connect(resistor.n, currentSensor.p) annotation (Line(
      points={{40,40},{40,-10},{30,-10}},
                                       color={0,0,255}));
  connect(capacitor.p, resistor.p) annotation (Line(points={{0,10},{0,
          70},{40,70},{40,60}}, color={0,0,255}));
  annotation (
    experiment(
      StopTime=0.1,
      Interval=5e-05,
      Tolerance=1e-06),
    Documentation(info="<html>
<p>This example demonstrates the switching on of a resistive load operated by a step up chopper.
DC output voltage is equal to <code>1/(1 - dutyCycle)</code> times the input voltage.
Plot current <code>currentSensor.i</code>, averaged current <code>meanCurrent.y</code>, total voltage <code>voltageSensor.v</code> and voltage <code>meanVoltage.v</code>.</p>
</html>"));
end ChopperStepUp_R;
