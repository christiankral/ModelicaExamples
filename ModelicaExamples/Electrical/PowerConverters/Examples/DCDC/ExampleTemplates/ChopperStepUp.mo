within ModelicaExamples.Electrical.PowerConverters.Examples.DCDC.ExampleTemplates;
partial model ChopperStepUp "Step up chopper including control"
  import Modelica;
  extends Modelica.Electrical.PowerConverters.Icons.ExampleTemplate;
  parameter Modelica.SIunits.Frequency f=1000 "Switching frequency";
  parameter Modelica.SIunits.Voltage Vsource=100 "Source voltage";
  parameter Modelica.SIunits.Inductance Lsource=10e-3 "Source inductance";
  parameter Modelica.SIunits.Capacitance C=0.1e-3 "Smoothing capacitance";
  parameter Real dutyCycle=0.25 "Duty cycle";
  parameter Modelica.SIunits.Voltage V0=Vsource/(1 - dutyCycle) "No-load voltage";
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(final V=
        Vsource)
             annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-80,0})));
  Modelica.Electrical.PowerConverters.DCDC.ChopperStepUp   chopperStepUp(
      useHeatPort=false)
    annotation (Placement(transformation(extent={{-40,-10},{-20,10}})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor
    annotation (Placement(transformation(extent={{30,-20},{10,0}})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={60,10})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(
        transformation(extent={{-90,-40},{-70,-20}})));
  Modelica.Electrical.PowerConverters.DCDC.Control.SignalPWM signalPWM(
                                   final constantDutyCycle=dutyCycle, final f=f)
                                   annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        origin={-30,-60})));
  Modelica.Blocks.Math.Mean meanCurrent(f=f, x0=0) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={20,-44})));
  Modelica.Blocks.Math.Mean meanVoltage(f=f, x0=0) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        origin={90,10})));
  Modelica.Electrical.Analog.Basic.Inductor inductorSource(i(fixed=true, start=0), final L=Lsource) annotation (Placement(transformation(extent={{-70,0},{-50,20}})));
  Modelica.Electrical.Analog.Basic.Capacitor capacitor(C=C, v(fixed=true, start=V0)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={0,0})));
equation
  connect(constantVoltage.n, chopperStepUp.dc_n1) annotation (Line(points={{-80,
          -10},{-50,-10},{-50,-6},{-40,-6}}, color={0,0,255}));
  connect(voltageSensor.n, currentSensor.p) annotation (Line(
      points={{60,0},{60,-10},{30,-10}},
                                      color={0,0,255}));
  connect(constantVoltage.n, ground.p) annotation (Line(
      points={{-80,-10},{-80,-20}}, color={0,0,255}));
  connect(voltageSensor.v, meanVoltage.u) annotation (Line(
      points={{71,10},{78,10}}, color={0,0,127}));
  connect(currentSensor.i, meanCurrent.u) annotation (Line(
      points={{20,-21},{20,-32}},   color={0,0,127}));
  connect(signalPWM.fire, chopperStepUp.fire_p)
    annotation (Line(points={{-36,-49},{-36,-12}}, color={255,0,255}));
  connect(constantVoltage.p, inductorSource.p)
    annotation (Line(points={{-80,10},{-70,10}}, color={0,0,255}));
  connect(inductorSource.n, chopperStepUp.dc_p1)
    annotation (Line(points={{-50,10},{-50,6},{-40,6}}, color={0,0,255}));
  connect(voltageSensor.p, capacitor.p)
    annotation (Line(points={{60,20},{60,70},{0,70},{0,10}}, color={0,0,255}));
  connect(capacitor.p, chopperStepUp.dc_p2) annotation (Line(points={{0,10},{-10,
          10},{-10,6},{-20,6}}, color={0,0,255}));
  connect(chopperStepUp.dc_n2, capacitor.n) annotation (Line(points={{-20,-6},{-10,
          -6},{-10,-10},{0,-10}}, color={0,0,255}));
  connect(capacitor.n, currentSensor.n)
    annotation (Line(points={{0,-10},{10,-10}}, color={0,0,255}));
  annotation (Documentation(
        info="<html>
<p>Step up chopper example template including supply and sensors; load is not yet included</p>
</html>"));
end ChopperStepUp;
