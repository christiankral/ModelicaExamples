within ModelicaExamples.Electrical.PowerConverters.Examples.DCDC.ExampleTemplates;
partial model ChopperStepDown "Step down chopper including control"
  extends Modelica.Electrical.PowerConverters.Icons.ExampleTemplate;
  parameter Modelica.SIunits.Frequency f=1000 "Switching frequency";
  parameter Modelica.SIunits.Voltage Vsource=100 "Source voltage";
  parameter Real dutyCycle=0.25 "Duty cycle";
  parameter Modelica.SIunits.Voltage V0=Vsource*dutyCycle "No-load voltage";
  Modelica.Electrical.Analog.Sources.ConstantVoltage constantVoltage(final V=
        Vsource)
             annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-80,0})));
  Modelica.Electrical.PowerConverters.DCDC.ChopperStepDown
    chopperStepDown(useHeatPort=false)
    annotation (Placement(transformation(extent={{-60,-10},{-40,10}})));
  Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor
    annotation (Placement(transformation(extent={{0,-16},{-20,4}})));
  Modelica.Electrical.Analog.Sensors.VoltageSensor voltageSensor
    annotation (Placement(transformation(
        extent={{-10,10},{10,-10}},
        rotation=270,
        origin={60,10})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(
        transformation(extent={{-90,-40},{-70,-20}})));
  Modelica.Electrical.PowerConverters.DCDC.Control.SignalPWM signalPWM(
                                   final constantDutyCycle=dutyCycle, final f=f)
                                   annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        origin={-50,-60})));
  Modelica.Blocks.Math.Mean meanCurrent(f=f, x0=0) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-10,-40})));
  Modelica.Blocks.Math.Mean meanVoltage(f=f, x0=0) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        origin={90,10})));
equation
  connect(constantVoltage.p, chopperStepDown.dc_p1) annotation (Line(
      points={{-80,10},{-70,10},{-70,6},{-60,6}}, color={0,0,255}));
  connect(constantVoltage.n, chopperStepDown.dc_n1) annotation (Line(
      points={{-80,-10},{-70,-10},{-70,-6},{-60,-6}}, color={0,0,255}));
  connect(chopperStepDown.dc_p2, voltageSensor.p) annotation (Line(
      points={{-40,6},{-30,6},{-30,70},{60,70},{60,20}}, color={0,0,255}));
  connect(voltageSensor.n, currentSensor.p) annotation (Line(
      points={{60,0},{60,-6},{0,-6}}, color={0,0,255}));
  connect(currentSensor.n, chopperStepDown.dc_n2) annotation (Line(
      points={{-20,-6},{-40,-6}}, color={0,0,255}));
  connect(constantVoltage.n, ground.p) annotation (Line(
      points={{-80,-10},{-80,-20}}, color={0,0,255}));
  connect(voltageSensor.v, meanVoltage.u) annotation (Line(
      points={{71,10},{78,10}}, color={0,0,127}));
  connect(currentSensor.i, meanCurrent.u) annotation (Line(
      points={{-10,-17},{-10,-28}}, color={0,0,127}));
  connect(signalPWM.fire, chopperStepDown.fire_p) annotation (Line(
      points={{-56,-49},{-56,-12}}, color={255,0,255}));
  annotation (Documentation(
        info="<html>
<p>Step down chopper example template including supply and sensors; load is not yet included</p>
</html>"));
end ChopperStepDown;
