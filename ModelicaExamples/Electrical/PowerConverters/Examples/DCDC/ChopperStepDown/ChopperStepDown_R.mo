within ModelicaExamples.Electrical.PowerConverters.Examples.DCDC.ChopperStepDown;
model ChopperStepDown_R "Step down chopper with resistive load"
  extends ExampleTemplates.ChopperStepDown;
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Resistance R=100 "Resistance";
  Modelica.Electrical.Analog.Basic.Resistor resistor(R=R) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={30,50})));
equation
  connect(chopperStepDown.dc_p2, resistor.p) annotation (Line(
      points={{-40,6},{-30,6},{-30,70},{30,70},{30,60}}, color={0,0,255}));
  connect(resistor.n, currentSensor.p) annotation (Line(
      points={{30,40},{30,-6},{0,-6}}, color={0,0,255}));
  annotation (
    experiment(
      StartTime=0,
      StopTime=0.1,
      Tolerance=1e-06,
      Interval=0.0002),
    Documentation(info="<html>
<p>This example demonstrates the switching on of a resistive load operated by a step down chopper.
DC output voltage is equal to <code>dutyCycle</code> times the input voltage.
Plot current <code>currentSensor.i</code>, averaged current <code>meanCurrent.y</code>, total voltage <code>voltageSensor.v</code> and voltage <code>meanVoltage.v</code>.</p>
</html>"));
end ChopperStepDown_R;
