within ModelicaExamples.Electrical.PowerConverters.Examples.DCDC.ChopperStepDown;
model ChopperStepDown_RL "Step down chopper with R-L load"
  extends ExampleTemplates.ChopperStepDown;
  extends Modelica.Icons.Example;
  parameter Modelica.SIunits.Resistance R=100 "Resistance";
  parameter Modelica.SIunits.Inductance L=1 "Inductance";
  Modelica.Electrical.Analog.Basic.Resistor resistor(R=R) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={30,50})));
  Modelica.Electrical.Analog.Basic.Inductor inductor(L=L, i(fixed=true,
        start=0)) annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={30,10})));
equation
  connect(chopperStepDown.dc_p2, resistor.p) annotation (Line(
      points={{-40,6},{-30,6},{-30,70},{30,70},{30,60}}, color={0,0,255}));
  connect(resistor.n, inductor.p) annotation (Line(
      points={{30,40},{30,20}}, color={0,0,255}));
  connect(inductor.n, currentSensor.p) annotation (Line(
      points={{30,0},{30,-6},{0,-6}}, color={0,0,255}));
  annotation (
    experiment(
      StartTime=0,
      StopTime=0.1,
      Tolerance=1e-06,
      Interval=0.0002),
    Documentation(info="<html>
<p>This example demonstrates the switching on of an R-L load operated by a step down chopper.
DC output voltage is equal to <code>dutyCycle</code> times the input voltage.
Plot current <code>currentSensor.i</code>, averaged current <code>meanCurrent.y</code>, total voltage <code>voltageSensor.v</code> and voltage <code>meanVoltage.v</code>. The waveform the average current is determined by the time constant <code>L/R</code> of the load.</p>
</html>"));
end ChopperStepDown_RL;
