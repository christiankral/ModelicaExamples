within ModelicaExamples.Electrical.Machines.Examples;
package SynchronousInductionMachines "Test examples of synchronous induction machines"
  extends Modelica.Icons.ExamplesPackage;










  annotation (Documentation(info="<html>
This package contains test examples of synchronous induction machines.
</html>"));
end SynchronousInductionMachines;
