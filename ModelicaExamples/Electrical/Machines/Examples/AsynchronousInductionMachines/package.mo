within ModelicaExamples.Electrical.Machines.Examples;
package AsynchronousInductionMachines "Test examples of asynchronous induction machines"
  extends Modelica.Icons.ExamplesPackage;










  annotation (Documentation(info="<html>
This package contains test examples of asynchronous induction machines.
</html>"));
end AsynchronousInductionMachines;
