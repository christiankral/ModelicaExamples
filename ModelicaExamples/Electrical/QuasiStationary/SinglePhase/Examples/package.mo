within ModelicaExamples.Electrical.QuasiStationary.SinglePhase;
package Examples "Test examples"
  extends Modelica.Icons.ExamplesPackage;





  annotation (Documentation(info="<html>
Examples to demonstrate the usage of quasistationary electric components.
</html>"));
end Examples;
