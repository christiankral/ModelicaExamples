within ModelicaExamples.Electrical.QuasiStationary.MultiPhase.MultiPhase;
package Sources "AC multiphase sources"
  extends Modelica.Icons.SourcesPackage;








  annotation (Documentation(info="<html>
<p>This package hosts sources for quasi stationary multiphase circuits.
Quasi stationary theory can be found in the
<a href=\"modelica://Modelica.Electrical.QuasiStationary.UsersGuide.References\">references</a>.
</p>
<h4>See also</h4>

<a href=\"modelica://Modelica.Electrical.QuasiStationary.SinglePhase.Sources\">SinglePhase.Sources</a>

</html>"));
end Sources;
