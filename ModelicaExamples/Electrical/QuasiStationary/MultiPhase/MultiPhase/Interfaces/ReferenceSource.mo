within ModelicaExamples.Electrical.QuasiStationary.MultiPhase.MultiPhase.Interfaces;
partial model ReferenceSource
  "Partial of voltage or current source with reference input"
  extends ModelicaExamples.Electrical.QuasiStationary.MultiPhase.MultiPhase.Interfaces.OnePort;
  import Modelica.Constants.pi;
equation
  Connections.root(plug_p.reference);
  annotation (Icon(graphics={Ellipse(
              extent={{-50,50},{50,-50}},
              lineColor={85,170,255},
              fillColor={255,255,255},
              fillPattern=FillPattern.Solid),
        Line(points={{-90,0},{-50,0}}, color={85,170,255}),
        Line(points={{50,0},{90,0}}, color={85,170,255}),
        Text(
          extent={{160,-100},{-160,-60}},
          textString="m=%m"),
                           Text(
              extent={{-160,60},{160,100}},
              textString="%name",
              lineColor={0,0,255})}), Documentation(info="<html>
<p>
The source partial model relies on the
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Interfaces.TwoPlug\">TwoPlug</a> and contains a proper icon.
</p>

<h4>See also</h4>

<p>
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sources.VoltageSource\">VoltageSource</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sources.VariableVoltageSource\">VariableVoltageSource</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sources.CurrentSource\">CurrentSource</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sources.VariableCurrentSource\">VariableCurrentSource</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.SinglePhase.Interfaces.Source\">SinglePhase.Interfaces.Source</a>.
</p>
</html>"));
end ReferenceSource;
