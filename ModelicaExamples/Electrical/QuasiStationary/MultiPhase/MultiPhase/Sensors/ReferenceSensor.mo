within ModelicaExamples.Electrical.QuasiStationary.MultiPhase.MultiPhase.Sensors;
model ReferenceSensor "Sensor of reference angle gamma"
  extends ModelicaExamples.Electrical.QuasiStationary.MultiPhase.MultiPhase.Interfaces.AbsoluteSensor;
  Modelica.Blocks.Interfaces.RealOutput y "Reference angle" annotation (
      Placement(transformation(extent={{100,-10},{120,10}})));
equation
  y = plug_p.reference.gamma;
  plug_p.pin.i = fill(Complex(0), m);
  annotation (
    Icon(graphics={Text(
          extent={{60,-60},{-60,-30}},
          textString="ref")}), Documentation(info="<html>
<p>
This sensor can be used to measure the reference angle.
</p>

<h4>See also</h4>

<p>
<a href=\"modelica://Modelica.Electrical.QuasiStationary.SinglePhase.Sensors.ReferenceSensor\">SinglePhase.Sensors.ReferenceSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.FrequencySensor\">FrequencySensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.PotentialSensor\">PotentialSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.VoltageSensor\">VoltageSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.VoltageQuasiRMSSensor\">VoltageQuasiRMSSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.CurrentSensor\">CurrentSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.CurrentQuasiRMSSensor\">CurrentQuasiRMSSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.PowerSensor\">PowerSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.MultiSensor\">MultiSensor</a>
</p>

</html>"));
end ReferenceSensor;
