within ModelicaExamples.Electrical.QuasiStationary.MultiPhase.MultiPhase.Sensors;
model VoltageSensor "Voltage sensor"
  extends Interfaces.RelativeSensor;
  Modelica.Electrical.QuasiStationary.SinglePhase.Sensors.VoltageSensor voltageSensor[m] annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
  Modelica.SIunits.Voltage abs_y[m]=Modelica.ComplexMath.'abs'(y)
    "Magnitude of complex voltage";
  Modelica.SIunits.Angle arg_y[m]=Modelica.ComplexMath.arg(y)
    "Argument of complex voltage";

equation
  connect(plugToPins_p.pin_p, voltageSensor.pin_p) annotation (Line(points=
          {{-68,0},{-53.5,0},{-53.5,0},{-39,0},{-39,0},{-10,0}}, color={85,
          170,255}));
  connect(voltageSensor.pin_n, plugToPins_n.pin_n) annotation (Line(points=
          {{10,0},{39,0},{39,0},{68,0}}, color={85,170,255}));
  connect(voltageSensor.y, y) annotation (Line(points={{0,-11},{0,-35.75},{
          0,-35.75},{0,-60.5},{0,-60.5},{0,-110}}, color={85,170,255}));
  annotation (
    Icon(graphics={Text(
          extent={{-29,-11},{30,-70}},
          textString="V")}), Documentation(info="<html>

<p>
This sensor can be used to measure <em>m</em> complex voltages, using <em>m</em>
<a href=\"modelica://Modelica.Electrical.QuasiStationary.SinglePhase.Sensors.VoltageSensor\">single phase VoltageSensors</a>.
</p>

<h4>See also</h4>

<p>
<a href=\"modelica://Modelica.Electrical.QuasiStationary.SinglePhase.Sensors.VoltageSensor\">SinglePhase.Sensors.VoltageSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.ReferenceSensor\">ReferenceSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.FrequencySensor\">FrequencySensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.PotentialSensor\">PotentialSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.VoltageQuasiRMSSensor\">VoltageQuasiRMSSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.CurrentSensor\">CurrentSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.CurrentQuasiRMSSensor\">CurrentQuasiRMSSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.PowerSensor\">PowerSensor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.MultiSensor\">MultiSensor</a>
</p>

</html>"));
end VoltageSensor;
