within ModelicaExamples.Electrical.QuasiStationary.MultiPhase.MultiPhase;
package Sensors "AC multiphase sensors"
  extends Modelica.Icons.SensorsPackage;











  annotation (Documentation(info="<html>
<p>This package hosts sensors for quasi stationary multiphase circuits.
Quasi stationary theory can be found in the
<a href=\"modelica://Modelica.Electrical.QuasiStationary.UsersGuide.References\">references</a>.
</p>
<h4>See also</h4>

<a href=\"modelica://Modelica.Electrical.QuasiStationary.SinglePhase.Sensors\">SinglePhase.Sensors</a>

</html>"));
end Sensors;
