within ModelicaExamples.Electrical.QuasiStationary.MultiPhase.MultiPhase.Basic;
model Capacitor "Multiphase linear capacitor"
  extends Interfaces.TwoPlug;
  parameter Modelica.SIunits.Capacitance C[m](start=fill(1, m))
    "Capacitances";
  Modelica.Electrical.QuasiStationary.SinglePhase.Basic.Capacitor capacitor[m](final C=C) annotation (Placement(transformation(extent={{-10,-10},{10,10}})));
equation
  connect(plugToPins_p.pin_p, capacitor.pin_p) annotation (Line(points={{-68,
          0},{-53.5,0},{-53.5,0},{-39,0},{-39,0},{-10,0}}, color={85,170,
          255}));
  connect(capacitor.pin_n, plugToPins_n.pin_n) annotation (Line(points={{10,
          0},{39,0},{39,0},{68,0}}, color={85,170,255}));
  annotation (Icon(graphics={Text(
              extent={{150,-80},{-150,-40}},
              textString="m=%m"),
        Text(
          extent={{-150,90},{150,50}},
              textString="%name",
          lineColor={0,0,255}),
        Line(
          points={{-6,28},{-6,-28}},
          color={0,128,255}),
        Line(
          points={{6,28},{6,-28}},
          color={0,128,255}),Line(points={{-90,0},{-6,0}}, color={85,170,255}),
          Line(points={{6,0},{90,0}}, color={85,170,255})}), Documentation(info="<html>
<p>
The linear capacitor connects the complex currents <code><u>i</u></code> with the complex
voltages <code><u>v</u></code> by <code><u>v</u>*j*&omega;*C = <u>i</u></code>,
using <code>m</code> <a href=\"modelica://Modelica.Electrical.QuasiStationary.SinglePhase.Basic.Capacitor\">single phase Capacitors</a>.
</p>

<h4>See also</h4>
<p>
<a href=\"modelica://Modelica.Electrical.QuasiStationary.SinglePhase.Basic.Capacitor\">Capacitor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.Resistor\">Resistor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.Conductor\">Conductor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.Inductor\">Inductor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.Impedance\">Impedance</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.Admittance\">Admittance</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.VariableResistor\">Variable resistor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.VariableConductor\">Variable conductor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.VariableCapacitor\">Variable capacitor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.VariableInductor\">Variable inductor</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.VariableImpedance\">Variable impedance</a>,
<a href=\"modelica://Modelica.Electrical.QuasiStationary.MultiPhase.Basic.VariableAdmittance\">Variable admittance</a>
</p>
</html>"));
end Capacitor;
